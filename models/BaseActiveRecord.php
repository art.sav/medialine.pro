<?php

namespace app\models;

use Yii;

/**
 * This is the base model ar class.
 *
 */
class BaseActiveRecord extends \yii\db\ActiveRecord
{

    public function beforeSave($insert)
    {

        if (!parent::beforeSave($insert)) {
            return false;
        }

        if($insert) {
            $this->{static::PK} = \app\components\Guid\Guid::make(static::PREFIX_GUID);
        }

        return true;
    }

}
