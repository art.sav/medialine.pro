<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property string $news_id
 * @property string $news_title
 * @property string $news_body
 *
 * @property RubricsNews[] $rubricsNews
 */
class News extends BaseActiveRecord
{
    const PK = 'news_id';
    const PREFIX_GUID = 'NW';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_title', 'news_body'], 'required'],
            [['news_id'], 'string', 'max' => 15],
            [['news_title', 'news_body'], 'string', 'max' => 255],
            [['news_id'], 'unique'],
        ];
    }

    /**
     * Gets query for [[RubricsNews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRubricsNews()
    {
        return $this->hasMany(RubricsNews::className(), ['news_id' => 'news_id']);
    }

    public function getRubrics()
    {
        return $this->hasMany(Rubrics::className(), ['rubric_id' => 'rubric_id'])
                    ->viaTable('rubrics_news', ['news_id' => 'news_id']);
                    //->via('rubricsNews');
    }


}
