<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News;
use \yii\db\Expression;

/**
 * NewsSearch represents the model behind the search form of `\app\models\News`.
 */
class NewsSearch extends News
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'news_title', 'news_body'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'news_id', $this->news_id])
            ->andFilterWhere(['like', 'news_title', $this->news_title])
            ->andFilterWhere(['like', 'news_body', $this->news_body]);

        return $dataProvider;
    }

    public function searchByRubric(Rubrics $rubric)
    {
        $queryChildRubric = Rubrics::find();
        $queryChildRubric->select(['rubric_id']);
        $queryChildRubric->from(['rubrics', new Expression(sprintf('(select @pv := \'%s\') initialisation',$rubric->rubric_id)) ]);
        $queryChildRubric->where(new Expression('find_in_set(rubric_parent_id, @pv)'))
                         ->andWhere(new Expression('length(@pv := concat(@pv, \',\', rubric_id))'));


        $newsQuery = News::find();
        $newsQuery->leftJoin('rubrics_news', '`rubrics_news`.`news_id` = `news`.`news_id`')
                  ->leftJoin('rubrics', '`rubrics`.`rubric_id` = `rubrics_news`.`rubric_id`');
        $newsQuery->where([
                    'or',
                    ['=','rubrics.rubric_id', $rubric->rubric_id],
                    ['in', 'rubrics.rubric_id', $queryChildRubric]
                ]);

        $newsQuery->with(['rubrics']);

        return $newsQuery->asArray()->all();
    }

}
