<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rubrics_news".
 *
 * @property string $rubrics_news_id
 * @property string $rubric_id
 * @property string $news_id
 *
 * @property News $news
 * @property Rubrics $rubric
 */
class RubricsNews extends BaseActiveRecord
{
    const PK = 'rubrics_news_id';
    const PREFIX_GUID = 'RN';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rubrics_news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rubric_id', 'news_id'], 'required'],
            [['rubrics_news_id', 'rubric_id', 'news_id'], 'string', 'max' => 15],
            [['rubrics_news_id'], 'unique'],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'news_id']],
            [['rubric_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubrics::className(), 'targetAttribute' => ['rubric_id' => 'rubric_id']],
        ];
    }

    /**
     * Gets query for [[News]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['news_id' => 'news_id']);
    }

    /**
     * Gets query for [[Rubric]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRubric()
    {
        return $this->hasOne(Rubrics::className(), ['rubric_id' => 'rubric_id']);
    }
}
