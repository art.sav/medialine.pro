<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rubrics".
 *
 * @property string $rubric_id
 * @property string|null $rubric_name
 * @property string|null $rubric_parent_id
 *
 * @property Rubrics $rubric
 * @property Rubrics $rubrics
 * @property Rubrics $rubricParent
 * @property Rubrics[] $rubrics0
 * @property RubricsNews[] $rubricsNews
 */
class Rubrics extends BaseActiveRecord
{
    const PK = 'rubric_id';
    const PREFIX_GUID = 'RB';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rubrics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rubric_id', 'rubric_parent_id'], 'string', 'max' => 15],
            [['rubric_name'], 'string', 'max' => 255],
            [['rubric_id'], 'unique'],
            [['rubric_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubrics::className(), 'targetAttribute' => ['rubric_id' => 'rubric_parent_id']],
            [['rubric_parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubrics::className(), 'targetAttribute' => ['rubric_parent_id' => 'rubric_id']],
        ];
    }

    /**
     * Gets query for [[Rubric]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRubric()
    {
        return $this->hasOne(Rubrics::className(), ['rubric_parent_id' => 'rubric_id']);
    }

    /**
     * Gets query for [[Rubrics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRubrics()
    {
        return $this->hasOne(Rubrics::className(), ['rubric_id' => 'rubric_parent_id']);
    }

    /**
     * Gets query for [[RubricParent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRubricParent()
    {
        return $this->hasOne(Rubrics::className(), ['rubric_id' => 'rubric_parent_id']);
    }

    /**
     * Gets query for [[Rubrics0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRubrics0()
    {
        return $this->hasMany(Rubrics::className(), ['rubric_parent_id' => 'rubric_id']);
    }

    /**
     * Gets query for [[RubricsNews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRubricsNews()
    {
        return $this->hasMany(RubricsNews::className(), ['rubric_id' => 'rubric_id']);
    }
}
