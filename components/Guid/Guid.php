<?php

namespace app\components\Guid;


class Guid
{

   static public function make($prefix = 'XX')
   {
      return uniqid($prefix);
   }

}
