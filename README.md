
TASK: TASK.md

Install:

composer install

./yii migrate

./yii fixture/load "News, Rubrics, RubricsNews"


Query:

GET /api/v1/news/by-rubric/RB0000000000001           - RB0000000000001  rubric ID

GET /api/v1/rubrics/hierarchy
