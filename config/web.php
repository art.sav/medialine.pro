<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'md0283hd0-90[1jsz081g9dv71gs8012gs9io]',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [ 'news' => 'news-rest' ],
                    'prefix' => 'api/v1',
                    'tokens' => [ '{news_id}' => '<id:(NW[0-9a-zA-Z]{13})>' ],
                    'patterns' => [
                            'PUT,PATCH {news_id}' => 'update',
                            'GET,HEAD by-rubric/<rubric_id:(RB[0-9a-zA-Z]{13})>' => 'by-rubric',
                            'DELETE {news_id}' => 'delete',
                            'GET,HEAD {news_id}' => 'view',
                            'POST' => 'create',
                            'GET,HEAD' => 'index',
                            '{news_id}' => 'options',
                            '' => 'options'
                        ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [ 'rubrics' => 'rubrics-rest' ],
                    'prefix' => 'api/v1',
                    'tokens' => [ '{rubric_id}' => '<id:(RB[0-9a-zA-Z]{13})>' ],
                    'patterns' => [
                            'PUT,PATCH {rubric_id}' => 'update',
                            'DELETE {rubric_id}' => 'delete',
                            'GET,HEAD {rubric_id}' => 'view',
                            'POST' => 'create',
                            'GET,HEAD hierarchy' => 'hierarchy',
                            'GET,HEAD' => 'index',
                            '{rubric_id}' => 'options',
                            '' => 'options'
                        ]
                ],
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
