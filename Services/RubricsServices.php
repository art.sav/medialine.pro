<?php

namespace app\Services;

class RubricsServices
{

    public function buildHierarchyByList(array $listRubrics): array
    {
        return $this->fetch_recursive(\yii\helpers\ArrayHelper::toArray($listRubrics));
    }

    public function fetch_recursive($source_array, $currentId = null)
    {
        $res = [];

        foreach ($source_array as $key => $rubric) {
            if($rubric['rubric_parent_id'] == $currentId) {

                $childs = $this->fetch_recursive($source_array, $rubric['rubric_id']);

                if(!empty($childs)) {
                    $rubric['childs'] = $childs;
                }

                $res[] = $rubric;
            }
        }
        return $res;
    }


}
