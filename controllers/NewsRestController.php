<?php

namespace app\controllers;

use Yii;
use \app\models\{Rubrics, NewsSearch};

class NewsRestController extends RestBaseController
{

    public $modelClass = 'app\models\News';

    public function actionByRubric($rubric_id)
    {
        $rubric = $this->loadRubric($rubric_id);

        return (new NewsSearch())->searchByRubric($rubric);
    }

    private function loadRubric($rubric_id): Rubrics
    {
        $rubric = Rubrics::findOne($rubric_id);

        if(!$rubric) {
            throw new \yii\web\NotFoundHttpException("Rubric ID not found", 1);
        }

        return $rubric;
    }

}
