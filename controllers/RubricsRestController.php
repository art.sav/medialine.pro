<?php

namespace app\controllers;

use Yii;
use \app\models\Rubrics;
use \app\Services\RubricsServices;

class RubricsRestController extends RestBaseController
{

    public $modelClass = 'app\models\Rubrics';


    public function actionHierarchy()
    {
        $allRubrics = Rubrics::find()->all();

        $rubricsService = new RubricsServices();

        $rubricHierarchy = $rubricsService->buildHierarchyByList($allRubrics);

        return $rubricHierarchy;
    }

}
