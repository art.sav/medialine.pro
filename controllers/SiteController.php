<?php

namespace app\controllers;

use Yii;

class SiteController extends \yii\web\Controller
{
    public function actionError()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->errorHandler->exception) {

    		\Yii::$app->response->setStatusCode(Yii::$app->errorHandler->exception->statusCode);

            return [
                'message' => \Yii::$app->errorHandler->exception->getMessage()
            ];

        }

        return '';
    }
}
