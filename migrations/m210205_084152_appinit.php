<?php

use yii\db\Migration;

/**
 * Class m210205_084152_appinit
 */
class m210205_084152_appinit extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // rubrics
        $this->createTable('{{%rubrics}}', [
            'rubric_id' => $this->string(15)->notNull(),
            'rubric_name' => $this->string(255)->null(),
            'rubric_parent_id' => $this->string(15)->null(),
            'PRIMARY KEY (rubric_id)',
        ]);

        // news
        $this->createTable('{{%news}}', [
            'news_id' => $this->string(15)->notNull(),
            'news_title' => $this->string(255)->notNull(),
            'news_body' => $this->text()->notNull(),
            'PRIMARY KEY (news_id)',
        ]);

        // rubrics_hierarchy
        /*
        $this->createTable('{{%rubrics_hierarchy}}', [
            'rubrics_hierarchy_id' => $this->string(15)->notNull(),
            'rubric_parent_id' => $this->string(15)->notNull(),
            'rubric_child_id' => $this->string(15)->notNull()
            'PRIMARY KEY (rubrics_hierarchy_id)',
        ]);*/

        // rubrics_news
        $this->createTable('{{%rubrics_news}}', [
            'rubrics_news_id' => $this->string(15)->notNull(),
            'rubric_id' => $this->string(15)->notNull(),
            'news_id' => $this->string(15)->notNull(),
            'PRIMARY KEY (rubrics_news_id)',
        ]);


        //$this->createIndex('rubrics_news_id', '{{%rubrics_news}}', 'news_id');

        $this->addForeignKey('fk_rubric_parent_id', '{{%rubrics}}', 'rubric_parent_id', '{{%rubrics}}', 'rubric_id', 'SET NULL', 'NO ACTION');
        $this->addForeignKey('fk_rubric_child_id', '{{%rubrics}}', 'rubric_id', '{{%rubrics}}', 'rubric_parent_id', 'NO ACTION', 'NO ACTION');

        $this->addForeignKey('fk_news_id', '{{%rubrics_news}}', 'news_id', '{{%news}}', 'news_id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_rubric_id', '{{%rubrics_news}}', 'rubric_id', '{{%rubrics}}', 'rubric_id', 'CASCADE', 'NO ACTION');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210205_084152_appinit cannot be reverted.\n";

        $this->dropForeignKey('fk_rubric_parent_id', '{{%rubrics}}');
        $this->dropForeignKey('fk_rubric_child_id', '{{%rubrics}}');
        $this->dropForeignKey('fk_news_id', '{{%news}}');
        $this->dropForeignKey('fk_rubric_id', '{{%rubrics}}');

        $this->dropTable('{{%rubrics}}');
        $this->dropTable('{{%news}}');
        $this->dropTable('{{%rubrics_hierarchy}}');
        $this->dropTable('{{%rubrics_news}}');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210205_084152_appinit cannot be reverted.\n";

        return false;
    }
    */
}
