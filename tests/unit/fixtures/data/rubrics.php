<?php

return [
    [
        'rubric_id' => 'RB0000000000001',
        'rubric_name' => 'Общество',
        'rubric_parent_id' => NULL,
    ],
    [
        'rubric_id' => 'RB0000000000002',
        'rubric_name' => 'городская жизнь',
        'rubric_parent_id' => 'RB0000000000001',
    ],
    [
        'rubric_id' => 'RB0000000000003',
        'rubric_name' => 'выборы',
        'rubric_parent_id' => 'RB0000000000001',
    ],
    [
        'rubric_id' => 'RB0000000000004',
        'rubric_name' => 'День города',
        'rubric_parent_id' => NULL,
    ],
    [
        'rubric_id' => 'RB0000000000005',
        'rubric_name' => 'салюты',
        'rubric_parent_id' => 'RB0000000000004',
    ],
    [
        'rubric_id' => 'RB0000000000006',
        'rubric_name' => 'детская площадка',
        'rubric_parent_id' => 'RB0000000000004',
    ],
    [
        'rubric_id' => 'RB0000000000007',
        'rubric_name' => '0-3 года',
        'rubric_parent_id' => 'RB0000000000006',
    ],
    [
        'rubric_id' => 'RB0000000000008',
        'rubric_name' => '3-7 года',
        'rubric_parent_id' => 'RB0000000000006',
    ],
    [
        'rubric_id' => 'RB0000000000009',
        'rubric_name' => 'Спорт',
        'rubric_parent_id' => NULL,
    ],

];
