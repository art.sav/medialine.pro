<?php

namespace tests\unit\fixtures;

use yii\test\ActiveFixture;

class RubricsFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Rubrics';
}
