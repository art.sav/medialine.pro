<?php

namespace tests\unit\fixtures;

use yii\test\ActiveFixture;

class RubricsNewsFixture extends ActiveFixture
{
    public $modelClass = 'app\models\RubricsNews';
}
